#!/bin/bash

usage() { echo "Usage: $0 [-n <name>][-m <model>] [-h <headmodel>] [-W <WIDTH>] [-H <HEIGHT>]" 1>&2; exit 1; }

SRC="$HOME/.q3a/baseq3/autoexec.nomodel.cfg"
DEST="$HOME/.q3a/baseq3/autoexec.cfg"

while getopts ":n:m:h:W:H:" option; do
    case "${option}" in
        n)
            n=${OPTARG}
            ;;
        m)
            m=${OPTARG}
            ;;
        h)
            h=${OPTARG}
            ;;
        W)
            W=${OPTARG}
            ;;
        H)
            H=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done
shift $((OPTIND-1))
#if [ -z "${m}" ] || [ -z "${h}" ] || [ -z "${W}" ] || [ -z "${H}" ]; then
#    usage
#fi

cat $SRC > $DEST

if [[ "${W}" ]]; then 
	echo "set r_customwidth \"${W}\"" >> $DEST 
fi
if [[ "${H}" ]]; then 
	echo "set r_customheight \"${H}\"" >> $DEST 
fi
if [[ "${n}" ]]; then 
	echo "set name \"${n}\"" >> $DEST 
fi
if [[ "${m}" ]]; then 
	echo "seta model \"${m}\"" >> $DEST 
	echo "seta team_model \"${m}\"" >> $DEST 
	if [[ -z "${h}" ]]; then 
    		echo "seta headmodel \"${m}\"" >> $DEST 
    		echo "seta team_headmodel \"${m}\"" >> $DEST 
	fi
fi
if [[ "${h}" ]]; then 
    echo "seta headmodel \"${h}\"" >> $DEST 
    echo "seta team_headmodel \"${h}\"" >> $DEST 
fi
quake3 &
exit 0

